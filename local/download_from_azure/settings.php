<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     local_download_from_azure
 * @category    admin
 * @copyright   2022 @korenerok <marvin.bernal@gmx.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
global $DB;


if($hassiteconfig){
    global $DB;

    $page = new admin_settingpage('local_download_from_azure', get_string('pluginname', 'local_download_from_azure'));

    $page->add(new admin_setting_configtext(
        'local_download_from_azure/downloadDirectory',
        get_string('downloadDirectory', 'local_download_from_azure'),
        get_string('downloadDirectoryDesc', 'local_download_from_azure'),
        '/var/www/moodledata/repository/azure',
        PARAM_RAW
    ));
 
    $ADMIN->add('localplugins', $page);
}