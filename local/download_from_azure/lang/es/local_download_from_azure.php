<?php

$string['pluginname'] = 'Descargar de Azure';
$string['download_from_azure'] = 'Descargar de Azure';
$string['not_writable_error'] = 'no tiene permisos de escritura para moodle, por favor cambie los permisos.';
$string['downloadDirectory'] = 'Carpeta destino';
$string['downloadDirectoryDesc'] = 'Carpeta que será usada para descargar del almacenamiento de Azure';

