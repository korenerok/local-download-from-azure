<?php

$string['pluginname'] = 'Download From Azure';
$string['download_from_azure'] = 'Download From Azure';
$string['not_writable_error'] = 'is not writable by moodle, please change folder permissions.';
$string['downloadDirectory'] = 'Download directory';
$string['downloadDirectoryDesc'] = 'Directory which will be used to download from Azure storage';
