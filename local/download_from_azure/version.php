<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version = 2022112200;
$plugin->component = 'local_download_from_azure';
$plugin->requires = 2020061501; // moodle 3.9
$plugin->dependencies = array(
    'local_azure_storage' => 2018072500,
    'tool_objectfs' => 2022070401
);