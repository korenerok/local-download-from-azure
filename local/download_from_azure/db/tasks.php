<?php

$tasks = [
    [
        'classname' => 'local_download_from_azure\task\download_from_azure',
        'blocking' => 0,
        'minute' => '30',
        'hour' => '22',
        'day' => '*',
        'month' => '*',
        'dayofweek' => '*',
    ]
];
