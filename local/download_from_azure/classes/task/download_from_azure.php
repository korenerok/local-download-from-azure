<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A scheduled task for synchronize activity of an user in the platform. Due to the cost of checking the standard 
 * log table, it is done through a task to store in a plugin table.
 *
 * @package     local_download_from_azure
 * @copyright   2022 @korenerok <marvin.bernal@gmx.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_download_from_azure\task;
#use local_download_from_azure\
require_once($CFG->dirroot . '/local/azure_storage/vendor/autoload.php');
use MicrosoftAzure\Storage\Common\ServicesBuilder;

class download_from_azure extends \core\task\scheduled_task {
    
    /**
     * Return the task's name as shown in admin screens.
     *
     * @return string
     */
    public function get_name() {
        return get_string('download_from_azure', 'local_download_from_azure');
    }
    
    private function clean_sastoken($sastoken)
    {
        if (substr($sastoken, 0, 1) === '?') {
            $sastoken = substr($sastoken, 1);
        }

        return $sastoken;
    }

    public function execute() {
        global $DB,$CFG;
        $sastoken= get_config('tool_objectfs','azure_sastoken');
        $azure_account= get_config('tool_objectfs','azure_accountname');
        $azure_container= get_config('tool_objectfs','azure_container');
        $sastoken= $sastoken = $this->clean_sastoken($sastoken);
        if ($azure_account) {
            $azure_account .= '.';
        }
        $sasconnectionstring = "BlobEndpoint=https://" .
            $azure_account .
            "blob.core.windows.net;SharedAccessSignature=" .
            $sastoken;
        
        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($sasconnectionstring);
        $blobResults = $blobRestProxy->listBlobs($azure_container);
        $blobs = $blobResults->getBlobs();

        $directory= get_config("local_download_from_azure", "downloadDirectory");

        if(!is_writable($directory)){
            $errorMessage=get_string("not_writable_error","local_download_from_azure");
            mtrace("$directory $errorMessage");
            exit;
        }

        foreach ($blobs as $blob) {
            $blobName = $blob->getName();

            $filename =$directory.'/'.$blobName;

            $blob= $blobRestProxy->getBlob($azure_container,$blobName);
            $isfolder=$blob->getMetadata()['hdi_isfolder']=="true";
            if($isfolder){
                mkdir($filename);
            }else{
                $fpointer = fopen($filename, 'w');
                ob_end_clean();
                fwrite($fpointer,stream_get_contents($blob->getContentStream()));
                fclose($fpointer);
            }
        }
    }


}
